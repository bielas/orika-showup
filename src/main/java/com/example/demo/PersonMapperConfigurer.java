package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class PersonMapperConfigurer extends MapperBase {

    public PersonMapperConfigurer() {
        super();
    }

    public PersonDto map(Person person, Class dest) {
        return (PersonDto) super.defaultMap(person, dest);
    }

}

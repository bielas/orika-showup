package com.example.demo;

import org.springframework.stereotype.Service;

@Service
public class Test {

    private final PersonMapperConfigurer personMapperConfigurer;

    public Test(PersonMapperConfigurer personMapperConfigurer) {
        this.personMapperConfigurer = personMapperConfigurer;
    }

    public void testMapper(){
        Person person = new Person("Michal", 25, true, Sex.MALE);
        System.out.println("person:" + person);

        PersonDto personDto = personMapperConfigurer.map(person, PersonDto.class);
        System.out.println("personDto:" + personDto);
    }
}

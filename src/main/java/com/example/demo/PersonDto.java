package com.example.demo;

public class PersonDto {
    private String name;
    private int age;
    private Boolean isAdult;
    private Sex sex;

    public PersonDto() {
    }

    public PersonDto(String name, int age, Boolean isAdult, Sex sex) {
        this.name = name;
        this.age = age;
        this.isAdult = isAdult;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getAdult() {
        return isAdult;
    }

    public void setAdult(Boolean adult) {
        isAdult = adult;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "PersonDto{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", isAdult=" + isAdult +
                ", sex=" + sex +
                '}';
    }
}

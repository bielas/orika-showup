package com.example.demo;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;


public class MapperBase {
    private MapperFactory mapperFactory;

    public MapperBase() {
        mapperFactory = new DefaultMapperFactory.Builder().build();

    }

    public MapperFactory getMapperFactory() {
        return mapperFactory;
    }

    public MapperFacade getMapperFacade() {
        return mapperFactory.getMapperFacade();
    }

    public Object defaultMap(Object source, Class dest) {
        mapperFactory.classMap(source.getClass(), dest)
                .field("sex", "name")
                .byDefault()
                .register();
        return getMapperFacade().map(source, dest);
    }

}
